import unittest

from main import SimpleCalculator


class SimpleCalculatorTest(unittest.TestCase):

    def assertRaises(self, ValueError):
        pass

    def test_add_num(self):
        calculator = SimpleCalculator()
        self.assertEqual(calculator.addition(2, 7), 9)

    def test_add_two_num(self):
            calculator = SimpleCalculator()
            self.assertEqual(calculator.addition(1, 2, 3), 6)

    def test_add_multiple_num(self):
        numbers = range(10)
        calculator = SimpleCalculator()
        self.assertEqual(calculator.addition(*numbers), 55)

    def test_subtract_num(self):
        calculator = SimpleCalculator()
        self.assertEqual(calculator.subtract(12, 5), 7)

    def test_mul_num(self):
        calculator = SimpleCalculator()
        self.assertEqual(calculator.multiply(8, 9), 72)

    def test_mul_multiple_num(self):
        numbers = range(1, 10)
        calculator = SimpleCalculator()
        self.assertEqual(calculator.multiply(*numbers), 362880)

    def test_mul_by_zero(self):
        calculator = SimpleCalculator()

        self.assertRaises(calculator.multiply(7, 0))

    def test_div_num(self):
        calculator = SimpleCalculator()
        self.assertEqual(calculator.division(24, 5), 4.8)


    def test_div_by_zero(self):
         calculator = SimpleCalculator()
         self.assertEqual (calculator.division(9, 0), float('inf'))

if _name_ == 'main':
    unittest.main()