from functools import reduce


class SimpleCalculator:

    def addition(self, *args):
        return sum(args)

    def subtract(self, x, y):
        return x - y

    def multiply(self, *args):
        if not all(args):
            raise ValueError
        return reduce(lambda x, y: x * y, args)

    def division(self, x, y):
        try:
            return x / y
        except ZeroDivisionError:
            return float('inf')
